package br.com.itau.appcarteiradeinvestimentos.controllers;

import br.com.itau.appcarteiradeinvestimentos.models.Simulacao;
import br.com.itau.appcarteiradeinvestimentos.services.SimulacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/simulacoes")
public class SimulacaoController {

    @Autowired
    SimulacaoService simulacaoService;

    @GetMapping
    public Iterable<Simulacao> consultarListaSimulacoes() {
        Iterable<Simulacao> simulacoes = simulacaoService.consultarListaSimulacoes();
        return simulacoes;
    }
}
