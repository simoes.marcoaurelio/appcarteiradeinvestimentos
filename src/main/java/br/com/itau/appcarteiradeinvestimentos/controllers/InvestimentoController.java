package br.com.itau.appcarteiradeinvestimentos.controllers;

import br.com.itau.appcarteiradeinvestimentos.dtos.RespostaDTO;
import br.com.itau.appcarteiradeinvestimentos.models.Investimento;
import br.com.itau.appcarteiradeinvestimentos.models.Simulacao;
import br.com.itau.appcarteiradeinvestimentos.services.InvestimentoServices;
import br.com.itau.appcarteiradeinvestimentos.services.SimulacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/investimentos")
public class InvestimentoController {

    @Autowired
    private InvestimentoServices investimentoServices;

    @Autowired
    private SimulacaoService simulacaoService;



    //    POST - COMANDOS POSTMAN
    //
    //    localhost:8080/investimentos
    //
    //    {
    //        "nomeInvestimento": "<nome>",
    //        "rendimentoAoMes": <valor>
    //    }

    @PostMapping
    public Investimento gravarInvestimento(@RequestBody Investimento investimento) {
        return investimentoServices.gravarInvestimento(investimento);
    }



    //    GET - COMANDOS POSTMAN
    //
    //    localhost:8080/investimentos
    //    localhost:8080/investimentos?nomeInvestimento=<parte do nome>
    //    localhost:8080/investimentos?rendimentoAoMes=<rendimento>

    @GetMapping
    public Iterable<Investimento> consultarListaInvestimentos
            (@RequestParam(name = "nomeInvestimento", required = false) String nomeInvestimento,
             @RequestParam(name = "rendimentoAoMes", required = false) Double rendimentoAoMes) {

        if (nomeInvestimento != null) {
            Iterable<Investimento> investimentos = investimentoServices.consultarInvestimentoPorNome(nomeInvestimento);
            return investimentos;
        }

        if (rendimentoAoMes != null) {
            Iterable<Investimento> investimentos =
                    investimentoServices.consultarInvestimentoPorRendimentoAoMes(rendimentoAoMes);
            return investimentos;
        }

        Iterable<Investimento> investimentos = investimentoServices.consultarListaInvestimentos();
        return investimentos;
    }



    //    GET - COMANDOS POSTMAN
    //
    //    localhost:8080/investimentos/1

    @GetMapping("/{idInvestimento}")
    public Investimento consultarInvestimentoPorId(@PathVariable(name = "idInvestimento") int idInvestimento) {
        try {
            Investimento investimento = investimentoServices.consultarInvestimentoPorId(idInvestimento);
            return investimento;
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }



    //    GET - COMANDOS POSTMAN
    //
    //    localhost:8080/investimentos/1

    @DeleteMapping("/{idInvestimento}")
    public ResponseEntity<?> deletarInvestimento(@PathVariable(name = "idInvestimento") int idInvestimento) {
        try {
            investimentoServices.deletarInvestimento(idInvestimento);
            return ResponseEntity.status(204).body("");
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }



    //    GET - COMANDOS POSTMAN
    //
    //    localhost:8080/investimentos/<id>/simulacao
    //
    //    {
    //        "nomeInteressado": "<nome>",
    //        "emailInteressado": "<email>",
    //        "valorAplicacao": <valor>,
    //        "quantidadeMeses": <quantidade>
    //    }

    @PostMapping("/{idInvestimento}/simulacao")
    public RespostaDTO simularInvestimento(@PathVariable(name = "idInvestimento") int idInvestimento,
                     @RequestBody Simulacao simulacao) {

        RespostaDTO resposta = simulacaoService.simularInvestimento(idInvestimento, simulacao);
        return resposta;
    }


}
