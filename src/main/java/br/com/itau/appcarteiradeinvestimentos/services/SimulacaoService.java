package br.com.itau.appcarteiradeinvestimentos.services;

import br.com.itau.appcarteiradeinvestimentos.dtos.RespostaDTO;
import br.com.itau.appcarteiradeinvestimentos.models.Investimento;
import br.com.itau.appcarteiradeinvestimentos.models.Simulacao;
import br.com.itau.appcarteiradeinvestimentos.repositories.SimulacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.time.LocalDate;

@Service
public class SimulacaoService {

    @Autowired
    SimulacaoRepository simulacaoRepository;

    @Autowired
    InvestimentoServices investimentoServices;

    // Simular investimento com valor e prazo de aplicação
    public RespostaDTO simularInvestimento(int idInvestimento, Simulacao simulacao) {

        Investimento investimento = investimentoServices.consultarInvestimentoPorId(idInvestimento);
        simulacao.setProdutoInvestimento(investimento);

        double valorInvestido = simulacao.getValorAplicacao();
        int periodo = simulacao.getQuantidadeMeses();
        double rendimento = investimento.getRendimentoAoMes();

        for (int i = 1; i <= periodo; i++) {
            valorInvestido += (valorInvestido * rendimento) / 100;
        }

        LocalDate data = LocalDate.now();
        simulacao.setDataSimulacao(data);

        Simulacao simulacaoObjeto = simulacaoRepository.save(simulacao);

        DecimalFormat campoDecimal = new DecimalFormat("0.00");

        RespostaDTO respostaDTO = new RespostaDTO();
        respostaDTO.setRendimentoPorMes(rendimento);
        respostaDTO.setMontante(campoDecimal.format(valorInvestido));

        return respostaDTO;
    }

    // Consultar todas simulações efetuadas
    public Iterable<Simulacao> consultarListaSimulacoes() {
        Iterable<Simulacao> simulacoes = simulacaoRepository.findAll();
        return simulacoes;
    }

}
