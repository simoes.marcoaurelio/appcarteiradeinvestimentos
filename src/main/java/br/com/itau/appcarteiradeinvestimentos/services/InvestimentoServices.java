package br.com.itau.appcarteiradeinvestimentos.services;

import br.com.itau.appcarteiradeinvestimentos.models.Investimento;
import br.com.itau.appcarteiradeinvestimentos.repositories.InvestimentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Optional;

@Service
public class InvestimentoServices {

    @Autowired
    private InvestimentoRepository investimentoRepository;

    // Gravar novo investimento
    public Investimento gravarInvestimento(Investimento investimento) {

        LocalDate data = LocalDate.now();
        investimento.setDataDeCadastroInvestimento(data);

        Investimento investimentoObjeto = investimentoRepository.save(investimento);
        return investimentoObjeto;
    }

    // Consultar lista de investimentos
    public Iterable<Investimento> consultarListaInvestimentos() {
        Iterable<Investimento> investimentos = investimentoRepository.findAll();
        return investimentos;
    }

    // Consultar investimentos por nome ou parte do nome
    public Iterable<Investimento> consultarInvestimentoPorNome(String nomeInvestimento) {
        Iterable<Investimento> investimentos =
                investimentoRepository.findAllByNomeInvestimentoContains(nomeInvestimento);
        return investimentos;
    }

    //Consultar investimentos por rendimento ao mês
    public Iterable<Investimento> consultarInvestimentoPorRendimentoAoMes(double rendimentoAoMes) {
        Iterable<Investimento> investimentos = investimentoRepository.findAllByRendimentoAoMes(rendimentoAoMes);
        return investimentos;
    }

    //Consultar investimento por ID
    public Investimento consultarInvestimentoPorId(int idInvestimento) {
        Optional<Investimento> optionalInvestimento = investimentoRepository.findById(idInvestimento);
        if (optionalInvestimento.isPresent()) {
            return optionalInvestimento.get();
        }
        throw new RuntimeException("O investimento não foi encontrado");
    }

    // Deletar investimento por ID
    public void deletarInvestimento(int idInvestimento) {
        if (investimentoRepository.existsById(idInvestimento)) {
            investimentoRepository.deleteById(idInvestimento);
        } else
            throw new RuntimeException("O investimento não foi encontrado");
    }
}
