package br.com.itau.appcarteiradeinvestimentos.dtos;

public class RespostaDTO {

    private double rendimentoPorMes;
    private String montante;

    public RespostaDTO() {
    }

    public double getRendimentoPorMes() {
        return rendimentoPorMes;
    }

    public void setRendimentoPorMes(double rendimentoPorMes) {
        this.rendimentoPorMes = rendimentoPorMes;
    }

    public String getMontante() {
        return montante;
    }

    public void setMontante(String montante) {
        this.montante = montante;
    }
}
