package br.com.itau.appcarteiradeinvestimentos.repositories;

import br.com.itau.appcarteiradeinvestimentos.models.Investimento;
import org.springframework.data.repository.CrudRepository;

public interface InvestimentoRepository extends CrudRepository<Investimento, Integer> {
    Iterable<Investimento> findAllByNomeInvestimentoContains(String nomeInvestimento);
    Iterable<Investimento> findAllByRendimentoAoMes(double rendimentoAoMes);
}
