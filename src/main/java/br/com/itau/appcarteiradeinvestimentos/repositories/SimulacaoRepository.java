package br.com.itau.appcarteiradeinvestimentos.repositories;

import br.com.itau.appcarteiradeinvestimentos.models.Simulacao;
import org.springframework.data.repository.CrudRepository;

public interface SimulacaoRepository extends CrudRepository<Simulacao, Integer> {
}
