package br.com.itau.appcarteiradeinvestimentos.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.time.LocalDate;

@Entity
@JsonIgnoreProperties(value = {"dataDeCadastro"}, allowGetters = true)
public class Investimento {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idInvestimento;

    @NotBlank(message = "O nome do investimento precisa ser preenchido")
    @Size(min = 5, max = 100, message = "o nome do investimento deve ter entre 5 e 100 caracteres")
    @Column(unique = true)
    private String nomeInvestimento;

    @DecimalMin(value = "0.1", message = "O valor do rendimento precisa ser maior que zero")
    private double rendimentoAoMes;

    private LocalDate dataDeCadastroInvestimento;

    public Investimento () {
    }

    public int getIdInvestimento() {
        return idInvestimento;
    }

    public void setIdInvestimento(int idInvestimento) {
        this.idInvestimento = idInvestimento;
    }

    public String getNomeInvestimento() {
        return nomeInvestimento;
    }

    public void setNomeInvestimento(String nomeInvestimento) {
        this.nomeInvestimento = nomeInvestimento;
    }

    public double getRendimentoAoMes() {
        return rendimentoAoMes;
    }

    public void setRendimentoAoMes(double rendimentoAoMes) {
        this.rendimentoAoMes = rendimentoAoMes;
    }

    public LocalDate getDataDeCadastroInvestimento() {
        return dataDeCadastroInvestimento;
    }

    public void setDataDeCadastroInvestimento(LocalDate dataDeCadastroInvestimento) {
        this.dataDeCadastroInvestimento = dataDeCadastroInvestimento;
    }
}
