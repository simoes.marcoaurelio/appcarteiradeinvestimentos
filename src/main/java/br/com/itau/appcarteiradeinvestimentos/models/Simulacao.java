package br.com.itau.appcarteiradeinvestimentos.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.validator.constraints.Range;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.time.LocalDate;

@Entity
@JsonIgnoreProperties(value = {"dataSimulacao"}, allowGetters = true)
public class Simulacao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idSimulacao;

    @NotBlank(message = "O nome do interessado precisa ser preenchido")
    @Size(min = 2, max = 100, message = "o nome do interessado deve ter entre 5 e 100 caracteres")
    private String nomeInteressado;

    @Email(message = "O formato do email não é válido")
    @NotNull(message = "O email não pode ser nulo")
    private String emailInteressado;

    @DecimalMin(value = "10.0", message = "O valor mínimo do investimento precisa ser maior que 10 reais")
    @Digits(message = "O valor simulado deve ter apenas duas casas após a vírgula", integer = 9999, fraction = 2)
    private double valorAplicacao;

    @Range(min = 2, max = 180, message = "O período do investimento precisa estar entre 2 e 180 meses")
    private int quantidadeMeses;

    private LocalDate dataSimulacao;

    @ManyToOne(cascade = CascadeType.ALL)
    private Investimento produtoInvestimento;

    public Simulacao() {
    }

    public int getIdSimulacao() {
        return idSimulacao;
    }

    public void setIdSimulacao(int idSimulacao) {
        this.idSimulacao = idSimulacao;
    }

    public String getNomeInteressado() {
        return nomeInteressado;
    }

    public void setNomeInteressado(String nomeInteressado) {
        this.nomeInteressado = nomeInteressado;
    }

    public String getEmailInteressado() {
        return emailInteressado;
    }

    public void setEmailInteressado(String emailInteressado) {
        this.emailInteressado = emailInteressado;
    }

    public double getValorAplicacao() {
        return valorAplicacao;
    }

    public void setValorAplicacao(double valorAplicacao) {
        this.valorAplicacao = valorAplicacao;
    }

    public int getQuantidadeMeses() {
        return quantidadeMeses;
    }

    public void setQuantidadeMeses(int quantidadeMeses) {
        this.quantidadeMeses = quantidadeMeses;
    }

    public LocalDate getDataSimulacao() {
        return dataSimulacao;
    }

    public void setDataSimulacao(LocalDate dataSimulacao) {
        this.dataSimulacao = dataSimulacao;
    }

    public Investimento getProdutoInvestimento() {
        return produtoInvestimento;
    }

    public void setProdutoInvestimento(Investimento produtoInvestimento) {
        this.produtoInvestimento = produtoInvestimento;
    }
}
