package br.com.itau.appcarteiradeinvestimentos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppcarteiradeinvestimentosApplication {

	public static void main(String[] args) {
		SpringApplication.run(AppcarteiradeinvestimentosApplication.class, args);
	}

}
